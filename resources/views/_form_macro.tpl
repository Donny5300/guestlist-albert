{% macro static(title, text, context, optionals) %}
	{% import _self as forms %}

	{{ forms.label(context, _context) }}
	<p class="form-control-static">{{ text }}</p>
	{{ forms._label(context, _context) }}
{% endmacro %}

{% macro input_text(title, name, value, context, optionals) %}
	{% import _self as forms %}

	{{ forms.label(context, _context) }}

	{% if optionals.icon_prepend is not empty or optionals.text_prepend is not empty or optionals.icon_append is not empty or optionals.text_append is not empty %}
		<div class="input-group">
	{% endif %}

	{% if optionals.icon_prepend is not empty or optionals.text_prepend is not empty %}
		<span class="input-group-addon"><i class="fa {{ optionals.icon_prepend }}"></i> {{ optionals.text_prepend }}</span>
	{% endif %}

	{% set ngInit = optionals.ngModel and Old(name, value) ? "ng-init="~optionals.ngInit~"('"~optionals.ngModel~"','"~Old(name, value)~"')" | raw %}

	<input type="{{ optionals.type|default('text') }}" {{ optionals.readOnly ? 'readonly' }} {{ optionals.disabled ? 'disabled' }} name="{{ name }}" class="form-control {{ optionals.classes }}" id="{{ optionals.id|default(name) }}" {{ optionals.functions }} value="{{ Old(name, value) }}" placeholder="{{ title|t }}" {{ optionals.ngModel ? 'ng-model='~(optionals.ngModel)  | raw}} {{ ngInit and optionals.ngInit? ngInit | raw }}>

	{% if optionals.icon_append is not empty or optionals.text_append is not empty %}
		<span class="input-group-addon"><i class="fa {{ optionals.icon_append }}"></i> {{ optionals.text_append }}</span>
	{% endif %}

	{% if optionals.help_text is not empty %}
		<span class="help-block m-b-none">{{ optionals.help_text|t }}</span>
	{% endif %}

	{% if optionals.icon_prepend is not empty or optionals.text_prepend is not empty or optionals.icon_append is not empty or optionals.text_append is not empty %}
		</div>
	{% endif %}

	{{ forms._label(context, _context) }}
{% endmacro %}

{% macro input_date(title, name, value, context, optionals) %}
	{% import _self as forms %}

	{{ forms.label(context, _context) }}

	{% if optionals.icon_prepend is not empty or optionals.text_prepend is not empty or optionals.icon_append is not empty or optionals.text_append is not empty %}
		<div class="input-group">
	{% endif %}

	{% if optionals.icon_prepend is not empty or optionals.text_prepend is not empty %}
		<span class="input-group-addon"><i class="fa {{ optionals.icon_prepend }}"></i> {{ optionals.text_prepend }}</span>
	{% endif %}

	{% set ngInit = optionals.ngModel and Old(name, value) ? "ng-init="~optionals.ngInit~"('"~optionals.ngModel~"','"~Old(name, value)~"')" | raw %}

	<input type="{{ optionals.type|default('text') }}" {{ optionals.readOnly ? 'readonly' }} {{ optionals.disabled ? 'disabled' }} name="{{ name }}" class="form-control datepicker {{ optionals.classes }}" id="{{ optionals.id|default(name) }}" {{ optionals.functions }} value="{{ Old(name, value) }}" placeholder="{{ title|t }}" {{ optionals.ngModel ? 'ng-model='~(optionals.ngModel)  | raw}} {{ ngInit and optionals.ngInit? ngInit | raw }}>

	{% if optionals.icon_append is not empty or optionals.text_append is not empty %}
		<span class="input-group-addon"><i class="fa {{ optionals.icon_append }}"></i> {{ optionals.text_append }}</span>
	{% endif %}

	{% if optionals.help_text is not empty %}
		<span class="help-block m-b-none">{{ optionals.help_text|t }}</span>
	{% endif %}

	{% if optionals.icon_prepend is not empty or optionals.text_prepend is not empty or optionals.icon_append is not empty or optionals.text_append is not empty %}
		</div>
	{% endif %}

	{{ forms._label(context, _context) }}
{% endmacro %}

{% macro input_hidden(name, value, optionals) %}
	<input name="{{ name }}" id="{{ optionals.id|default(name) }}" class="{{ optionals.class|default(name) }}" type="hidden" value="{{ Old(name, value) }}" {{ optionals.ngModel ? 'ng-model='~(optionals.ngModel) | raw }}>
{% endmacro %}

{% macro input_file(title, name, value, context, optionals) %}
	{% import _self as forms %}

	{{ forms.label(context, _context) }}
	<input name="{{ name }}" class="form-control {{ optionals.classes }}" id="{{ optionals.id|default(name) }}" {{ optionals.functions }} type="file" value="{{ Old(name, value) }}" placeholder="{{ title|t }}">
	{{ forms._label(context, _context) }}
{% endmacro %}

{% macro checkbox(title, name, values, items, context, optionals) %}
	{% import _self as forms %}

	{{ forms.label(context, _context) }}

	{% for item in items %}
		<div class="checkbox">
		<input id="{{ name~'-'~item.value }}" name="{{ name }}[]" type="checkbox" {{ optionals.functions }} {% if Old(name, values) is not empty and item.value in Old(name, values) %}checked="checked"{% endif %} value="{{ item.value }}">
		<label for="{{ name~'-'~item.value }}">
			{{ item.title }}
		</label>
	</div>
	{% endfor %}

	{{ forms._label(context, _context) }}
{% endmacro %}

{% macro single_checkbox(title, name, value, item, context, optionals) %}
	{% import _self as forms %}

	{{ forms.label(context, _context) }}

	<div class="checkbox">
		<input id="{{ name~'-'~value }}" name="{{ name }}" type="checkbox" {{ optionals.functions }} {% if value == item %}checked="checked"{% endif %} value="{{ value }}">
		<label for="{{ name~'-'~value }}">
			{{ item.title }}
		</label>
	</div>

	{{ forms._label(context, _context) }}
{% endmacro %}

{% macro list(title, name, value, items, context, optionals, extras) %}
	{% import _self as forms %}

	{{ forms.label(context, _context) }}

	<select id="{{ optionals.id|default(name) }}" name="{{ name }}" class="form-control {{ optionals.classes }}" {{ optionals.functions }} {% if optionals.disabled == true %}disabled="disabled"{% endif %}>
			{% for item in extras %}
				<option value="{{ item.value|default(item.id) }}" {% if item.disabled %}disabled="disabled"{% endif %} {% if (Old(name, value) is empty and loop.first) or Old(name, value) == item.value|default(item.id) %}selected="selected"{% endif %}>{{ item.title }}</option>
			{% endfor %}
		{% for item in items %}
			<option value="{{ item.value|default(item.id) }}" {% if item.disabled %}disabled="disabled"{% endif %} {% if (Old(name, value) is empty and loop.first and extras is not defined) or Old(name, value) == item.value|default(item.id) %}selected="selected"{% endif %}>{{ item.title }}</option>
		{% endfor %}
		</select>

	{{ forms._label(context, _context) }}
{% endmacro %}

{% macro yesno_list(title, name, value, context, optionals) %}
	{% import _self as forms %}

	{{ forms.label(context, _context) }}

	<select id="{{ optionals.id|default(name) }}" name="{{ name }}" class="form-control {{ optionals.classes }}" {{ optionals.functions }} {% if optionals.disabled == true %}disabled="disabled"{% endif %}>
			<option value="0" {% if (Old(name, value) is empty and loop.first and extras is not defined) or Old(name, value) == 0 %}selected="selected"{% endif %}>{{ 'form.option.no'|t }}</option>
			<option value="1" {% if Old(name, value) == 1 %}selected="selected"{% endif %}>{{ 'form.option.yes'|t }}</option>
		</select>

	{{ forms._label(context, _context) }}
{% endmacro %}

{% macro textarea(title, name, value, context, optionals) %}
	{% import _self as forms %}

	{{ forms.label(context, _context) }}

	<textarea name="{{ name }}" id="{{ optionals.id|default(name) }}" class="form-control {{ optionals.classes }}" {{ optionals.functions }} cols="15" rows="5">{{ Old(name, value) }}</textarea>

	{{ forms._label(context, _context) }}
{% endmacro %}

{% macro textarea_only(title, name, value, context, optionals) %}
	<textarea name="{{ name }}" id="{{ optionals.id|default(name) }}" class="form-control {{ optionals.classes }}" {{ optionals.functions }} cols="15" rows="{{ optionals.rows|default(5) }}">{{ Old(name, value) }}</textarea>
{% endmacro %}

{% macro wysiwyg(title, name, value, context, optionals) %}
	{% import _self as forms %}

	{{ forms.label(context, _context) }}

	<textarea name="{{ name }}" id="{{ optionals.id|default(name) }}" class="form-control {{ optionals.classes }}" {{ optionals.functions }} cols="15" rows="5">{{ Old(name, value) }}</textarea>

	{{ forms._label(context, _context) }}
{% endmacro %}

{% macro editor(title, name, value, context, optionals) %}
	{% import _self as forms %}

	{% set col_class = 'col-md-8' %}

	{{ forms.label(context, _context) }}
	{#<div class="form-editor" name="{{ name }}">
		{{ Old(name, value) }}
	</div>#}
	<textarea name="{{ name }}" id="{{ optionals.id|default(name) }}" class="form-editor {{ optionals.classes }}">{{ Old(name, value) }}</textarea>

	{{ forms._label(context, _context) }}
{% endmacro %}


{% macro depend_list(title, name, value, subvalue, items, context, optionals) %}
	{% import _self as forms %}

	{{ forms.label(context, _context) }}
	<div class="row">
		<div class="col-md-6">
			<select id="{{ optionals.id|default(name) }}" class="form-control depend-list" name="{{ name }}" {{ optionals.functions }} {% if optionals.disabled == true %}disabled="disabled"{% endif %}>
			{% for item in items %}
				<option value="{{ item.value }}" {% if value == item.value %}selected="selected"{% endif %}>{{ item.title }}</option>
			{% else %}
				<option value="0" {% if value == 0 %}selected="selected"{% endif %}>{{ optionals.empty|default('form.depend_list.empty.main'|t) }}</option>
			{% endfor %}
			</select>
		</div>

		<div class="col-md-6">
		{% for item in items %}
			<select name="{{ name }}_sub[{{ item.value }}]" class="form-control depend-list-target depend-list-target-{{ item.value }}{% if value is empty and subvalue is empty and loop.first %}{% elseif value != item.value %} hidden{% endif %}" {{ optionals.functions2 }}>
			{% for subitem in item[optionals.subkey|default('items')] %}
				<option value="{{ subitem.value }}" {% if subitem.disabled %}disabled="disabled"{% endif %} {% if subvalue == subitem.value %}selected="selected"{% endif %}>{{ subitem.title }}</option>
			{% else %}
				<option value="0" {% if subvalue == 0 %}selected="selected"{% endif %}>{{ optionals.empty_sub|default('form.depend_list.empty.sub'|t) }}</option>
			{% endfor %}
		</select>
		{% endfor %}
		</div>
	</div>
	{{ forms._label(context, _context) }}
{% endmacro %}

{% macro depend_list_sub(title, name, value, subvalue, subsubvalue, items, context, optionals) %}
	{% import _self as forms %}

	{{ forms.label(context, _context) }}
	<div class="row">
		<div class="col-md-4">
			<select id="{{ optionals.id|default(name) }}" class="form-control depend-list" name="{{ name }}" {{ optionals.functions }} {% if optionals.disabled == true %}disabled="disabled"{% endif %}>
				<option value="0" {% if optionals.allow_empty|default('false') == 'false' %}disabled="disabled"{% endif %} {% if items is not empty and value is empty %}selected="selected"{% endif %}>{{ optionals.select|default('select_' ~ name|t) }}</option>
				{% for item in items %}
					<option value="{{ item.value }}" {% if value == item.value %}selected="selected"{% endif %}>{{ item.title }}</option>
			{% else %}
					<option value="0" {% if value == 0 %}selected="selected"{% endif %}>{{ optionals.empty|default('item.'~context.controller~'.empty.main'|t) }}</option>
				{% endfor %}
			</select>
		</div>

		<div class="col-md-4">
		{% for item in items %}
			<select name="{{ name }}_sub[{{ item.value }}]" class="form-control depend-list-target depend-list-target-{{ item.value }}{% if value is empty and subvalue is empty and loop.first %}{% elseif value != item.value %} hidden{% endif %}" {{ optionals.functions2 }}>
			<option value="0" {% if optionals.allow_sub_empty|default('false') == 'false' %}disabled="disabled"{% endif %} {% if subvalue is empty %}selected="selected"{% endif %}>
				{{ optionals.select_sub_two|default('select_' ~ name ~ '_sub'|t) }}
			</option>
				{% for subitem in item[optionals.subkey|default('items')] %}
					<option value="{{ subitem.value }}" {% if subitem.disabled %}disabled="disabled"{% endif %} {% if subvalue == subitem.value %}selected="selected"{% endif %}>{{ subitem.title }}</option>
			{% else %}
					<option value="0" {% if subvalue == 0 %}selected="selected"{% endif %}>{{ optionals.empty_sub|default('item.'~context.controller~'.empty.sub'|t) }}</option>
				{% endfor %}
		</select>
		{% endfor %}
		</div>
		<div class="col-md-4">
		{% for item in items %}
			<select name="{{ name }}_sub_2[{{ item.value }}]" class="form-control depend-list-target depend-list-target-{{ item.value }}{% if value is empty and subvalue is empty and loop.first %}{% elseif value != item.value %} hidden{% endif %}" {{ optionals.functions2 }}>
			<option value="0" {% if optionals.allow_sub2_empty|default('false') == 'false' %}disabled="disabled"{% endif %} {% if subsubvalue is empty %}selected="selected"{% endif %}>
				{{ optionals.select_sub_two|default('select_' ~ name ~ '_sub_two'|t) }}
			</option>

				{% for subitem in item[optionals.subkey|default('items')] %}
					<option value="{{ subitem.value }}" {% if subitem.disabled %}disabled="disabled"{% endif %} {% if subsubvalue == subitem.value %}selected="selected"{% endif %}>{{ subitem.title }}</option>
			{% else %}
					<option value="0" {% if subvalue == 0 %}selected="selected"{% endif %}>{{ optionals.empty_sub|default('item.'~context.controller~'.empty.sub'|t) }}</option>
				{% endfor %}
		</select>
		{% endfor %}
		</div>
	</div>
	{{ forms._label(context, _context) }}
{% endmacro %}

{% macro label_online_status(status) %}
	{% if status %}
		<span class="label label-primary">{{ 'form.option.online'|t }}</span>
	{% else %}
		<span class="label label-danger">{{ 'form.option.offline'|t }}</span>
	{% endif %}
{% endmacro %}

{% macro label_boolean_status(status) %}
	{% if status %}
		<span class="label label-primary">{{ 'form.option.yes'|t }}</span>
	{% else %}
		<span class="label label-danger">{{ 'form.option.no'|t }}</span>
	{% endif %}
{% endmacro %}

{#
	These should always be included
#}
{% macro label(context, input) %}

{% set inputWidth = input.optionals.col_width|default(10) %}
{% set labelWidth = 12 - inputWidth %}

<div class="form-group {{ input.optionals.group_class }}">
	<label for="{{ input.optionals.id|default(input.name) }}" class="col-md-{{ labelWidth }} control-label">
		{{ input.title|t }}
	</label>
	<div class="{{ input.optionals.col_class|default('col-md-'~inputWidth) }}">
		{{ input.optionals.before }}
		{% endmacro %}

		{% macro _label(context, input) %}
		<span class="help-block m-b-none"><small>{{ input.optionals.after }}</small></span>
	</div>


</div>
<div class="hr-line-dashed"></div>
{% endmacro %}

{% macro form_control_static(title, value, context) %}
	{% import _self as forms %}

	{{ forms.label(context, _context) }}

	<div class="form-control-static">
		{{ value }}
	</div>

	{{ forms._label(context, _context) }}
{% endmacro %}

{% macro form_control_static_yes_no(title, value, context) %}
	{% import _self as forms %}

	{{ forms.label(context, _context) }}

	<div class="form-control-static">
		{{ value == 1 ? 'form.option.yes' | t : 'form.option.no' | t }}
	</div>

	{{ forms._label(context, _context) }}
{% endmacro %}