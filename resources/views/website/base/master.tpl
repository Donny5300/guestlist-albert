<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Derick Banks - Guest list</title>

    <!-- core CSS -->

    <link href="{{ url('template/css/bootstrap.min.css')}}" rel="stylesheet"/>
    <link href="{{ url('template/css/font-awesome.min.css')}}" rel="stylesheet"/>
    <link href="{{ url('template/css/animate.min.css')}}" rel="stylesheet"/>
    {#<link href="{{ url('template/css/prettyPhoto.css')}}" rel="stylesheet"/>#}
    <link href="{{ url('template/css/main.css')}}" rel="stylesheet"/>
    <link href="{{ url('template/css/responsive.css')}}" rel="stylesheet"/>
    <link href="{{ url('template/css/custom.css')}}" rel="stylesheet"/>
<script	src="{{ url('template/js/jquery.js')}}"></script>

    {#<script src="//code.jquery.com/jquery-1.10.2.js"></script>#}
    {#<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>#}
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    {#<link rel="shortcut icon" href="images/ico/favicon.ico">#}
    {#<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">#}
    {#<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">#}
    {#<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">#}
    {#<link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">#}
	{% import '_form_macro' as form %}

</head><!--/head-->

<body class="homepage">
<div id="fb-root"></div>
{#<script>(function(d, s, id) {#}
        {#var js, fjs = d.getElementsByTagName(s)[0];#}
        {#if (d.getElementById(id)) return;#}
        {#js = d.createElement(s); js.id = id;#}
        {#js.src = "//connect.facebook.net/nl_NL/sdk.js#xfbml=1&version=v2.3&appId=622770261118230";#}
        {#fjs.parentNode.insertBefore(js, fjs);#}
    {#}(document, 'script', 'facebook-jssdk'));</script>#}

<div class="hidden-print">
	{% include 'website.base.header' %}
</div>

{{ view | raw}}

<footer id="footer" class="midnight-blue">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                &copy; {{ 'now' | date('Y')}} Derick Banks. All Rights Reserved.
            </div>
            <div class="col-sm-6">
                {#{{--<ul class="pull-right">--}}#}
                {#{{--<li><a href="#">Home</a></li>--}}#}
                {#{{--<li><a href="#">About Us</a></li>--}}#}
                {#{{--<li><a href="#">Faq</a></li>--}}#}
                {#{{--<li><a href="#">Contact Us</a></li>--}}#}
                {#{{--</ul>--}}#}
            </div>
        </div>
    </div>
</footer><!--/#footer-->

<script	src="{{ url('template/js/bootstrap.min.js')}}"></script>
<script	src="{{ url('template/js/jquery.prettyPhoto.js')}}"></script>
<script	src="{{ url('template/js/jquery.isotope.min.js')}}"></script>
<script	src="{{ url('template/js/main.js')}}"></script>
{#<script	src="{{ url('template/js/wow.min.js')}}"></script>#}
</body>
</html>