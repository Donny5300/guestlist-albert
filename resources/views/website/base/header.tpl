<header id="header">
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xs-4">
                    <div class="top-number">
                        <p>
                            <i class="fa fa-envelope"></i> info.modernsociety@gmail.com

                        </p>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-8">
                    <div class="social">
                        <ul class="social-share">
                            <li><a href="https://www.facebook.com/events/1613349815553317/" target="_blank"><i
                                            class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/baiserevent" target="_blank"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="https://instagram.com/derickbanks/" target="_blank"><i
                                            class="fa fa-instagram"></i></a></li>
                            <li><a href="https://www.youtube.com/user/DerickBank" target="_blank"><i
                                            class="fa fa-youtube"></i></a></li>
                        </ul>
                        <div class="search">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/.container-->
    </div>
    <!--/.top-bar-->
    {% if auth_check() %}

        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="{{url('')}}">Home</a></li>


                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Events <i
                                        class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{url('admin/events/create')}}">Event aanmaken</a></li>
                                <li><a href="{{url('admin/events')}}">Overzicht</a></li>
                            </ul>
                        </li>
                        <li class="active"><a href="{{url('logout')}}">Logout</a></li>

                    </ul>
                </div>
            </div>
            <!--/.container-->
        </nav><!--/nav-->

    {% endif %}
</header><!--/header-->
