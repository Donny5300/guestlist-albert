<section id="front-page-block">
    <div class="front-page-background">
        <img src="{{ url('banners/image1.PNG') }}" class="img-responsive img-center" alt=""/> <br/>

    </div>
</section>

<section id="feature">
    <div class="container">

		<div class="row">
			<div class="col-sm-4 col-sm-offset-3">
				<div class="text-center">
					{% if session_has('message') %}
						{{ session('message') }}
					{% endif %}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<form action="/login" method="post" class="form-horizontal">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group">
						<label for="" class="control-label col-sm-2">Username</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="email">
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-sm-2">Password</label>
						<div class="col-sm-10">
							<input type="password" class="form-control" name="password">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-10 col-sm-offset-2">
							<button class="btn-default btn">Login</button>
						</div>
					</div>
				</form>
			</div>
		</div>



		<div class="row">
			<div class="col-sm-12 clearfix">
				<img src="{{ url('Critisizedlogofinal.png') }}" class="img-responsive pull-left" style="max-width: 100px;" alt=""/>
				<img src="{{ url('logo.png') }}" class="img-responsive pull-right" style="max-width: 100px; padding-top:30px" alt=""/>
			</div>

		</div>
		<!--/.row-->
	</div>
	<!--/.container-->
</section><!--/#feature-->