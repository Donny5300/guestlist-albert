
<section id="front-page-block">
    <div class="front-page-background">
        <img src="{{ url('banners/image1.PNG') }}" class="img-responsive img-center" alt=""/>
    </div>
</section>

<section id="feature">
    <div class="container">
        <div class="center wow fadeInDown">

            {% if not events %}
				<h2>Guestlist</h2>
				<p class="lead">Im sorry, but currently there isn't a guest list available!</p>
			{% else %}


				<p class="lead">Welcome to the guest list of Derick Banks. <br/>Fill in the form, follow me on Social Media
                    and see you when you see me ;)</p>

			{% endif %}

			{% if session('message')  %}
				{{ session('message') }}
			{% endif %}

			{% if errors.messages() %}
			<p class="lead">
					<ul style="list-style: none">
						{% for error in errors.messages() %}
						    <li>{{ error[0] }}</li>
						{% endfor %}
					</ul>
			</p>
			{% endif %}

        </div>

	<div class="row">
		<div class="col-sm-8 col-sm-offset-1">
			<div class="row">
				{% if events | length > 0 %}

					<form action="{{ url('/sign') }}" method="post" class="form-horizontal">
						{{ csrf_field() }}

						<div class="features">
							<div class="col-sm-12 wow fadeInUpBig" data-wow-duration="1000ms" data-wow-delay="600ms">
								<div class="feature-wrap">
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-6 control-label">
											Select event:
											<span class="asterisks">*</span>
										</label>

										<div class="col-sm-6">
											<select name="event_id" id="" class="form-control" title="Select a event">
												<option value="">Please make a choice</option>
												{% for event in events %}
													<option value="{{event.id}}">{{event.title}}</option>
												{% endfor %}
											</select>
										</div>
									</div>
								</div>
							</div>

							<div class="col-sm-12 wow fadeInUpBig" data-wow-duration="1000ms" data-wow-delay="600ms">
								<div class="feature-wrap">
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-6 control-label">
											Name:
											<span class="asterisks">*</span>
										</label>

										<div class="col-sm-6">

											<input type="text" name="name" class="form-control" placeholder="Your name" value="{{ Old('name') }}">
										</div>
									</div>
								</div>
							</div>

							<div class="col-sm-12 wow fadeInUpBig" data-wow-duration="1000ms" data-wow-delay="600ms">
								<div class="feature-wrap">
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-6 control-label">
											Last name:
											<span class="asterisks">*</span>
										</label>

										<div class="col-sm-6">
											<input type="text" name="last_name" class="form-control" placeholder="Your last name" value="{{ Old('last_name') }}">
										</div>
									</div>
								</div>
							</div>
							<!--/.col-md-4-->

							<div class="col-sm-12 wow fadeInUpBig" data-wow-duration="1000ms" data-wow-delay="600ms">
								<div class="feature-wrap">
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-6 control-label">
											Date of birth (dd-mm-YYYY):
											<span class="asterisks">*</span>
										</label>

										<div class="col-sm-6">
											<input type="text" name="birthday" class="form-control" placeholder="Your birthday" value="{{ Old('birthday') }}">
										</div>
									</div>
								</div>
							</div>
							<!--/.col-md-4-->

							<div class="col-sm-12 wow fadeInUpBig" data-wow-duration="1000ms" data-wow-delay="600ms">
								<div class="feature-wrap">
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-6 control-label">
											Phone number:
										</label>

										<div class="col-sm-6">
											<input type="text" name="phonenumber" class="form-control" placeholder="Your phone number" value="{{ Old('phonenumber') }}">
										</div>
									</div>
								</div>
							</div>
							<!--/.col-md-4-->

							<div class="col-sm-12 wow fadeInDownBig" data-wow-duration="1000ms" data-wow-delay="600ms">
								<div class="feature-wrap">
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-6 control-label">
											Gender:
											<span class="asterisks">*</span>
										</label>

										<div class="col-sm-6">
											<select name="gender" id="" class="form-control">
												<option value="">Select your gender</option>
												<option value="male" {{ Old('gender') == 'male' ? 'selected' }}>Male</option>
												<option value="female" {{ Old('gender') == 'female' ? 'selected' }}>Female</option>
											</select>
										</div>
									</div>
								</div>
							</div>

							<div class="col-sm-12 wow fadeInDownBig" data-wow-duration="1000ms" data-wow-delay="600ms">
								<div class="feature-wrap">
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-6 control-label">
											Email:
											<span class="asterisks">*</span>
										</label>

										<div class="col-sm-6">
											<input type="text" name="email" class="form-control" placeholder="Your email" value="{{ Old('email') }}">
										</div>
									</div>
								</div>
							</div>
							<!--/.col-md-4-->

							<div class="col-sm-12 wow fadeInDownBig" data-wow-duration="1000ms" data-wow-delay="600ms">
								<div class="feature-wrap">
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-6 control-label">
											How many people are joining you:
											<span class="asterisks">*</span>
										</label>

										<div class="col-sm-6">
											<select name="amount" id="" class="form-control">
												{% for i in 0..4 %}
													<option value="{{ i }}" {{ Old('amount') == i }}">{{ i }}</option>
												{% endfor %}
											</select>

										</div>
									</div>
								</div>
							</div>
							<!--/.col-md-4-->


							<div class="col-sm-12 wow fadeInDownBig" data-wow-duration="1000ms" data-wow-delay="600ms">
								<div class="feature-wrap">
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-6 control-label"></label>

										<div class="col-sm-6">
											<input type="submit" value="Save" class="btn btn-primary">
										</div>
									</div>
								</div>
							</div>
							<!--/.col-md-4-->
						</div>
						<!--/.services-->
					</form>
			{% else %}
					<p class="text-center">There are no guestlists available at this moment</p>
				{% endif %}

				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<img src="{{ url('Critisizedlogofinal.png') }}" class="img-responsive pull-left" style="max-width: 100px;" alt=""/>
				<img src="{{ url('logo.png') }}" class="img-responsive pull-right" style="max-width: 100px; padding-top:30px" alt=""/>
			</div>

		</div>
		<!--/.row-->
	</div>
	<!--/.container-->
</section><!--/#feature-->

<script>
   $(function ()
   {

	   $('input[type="submit"]').click(function ()
	   {
		   $(this).attr('disabled', 'disabled');
		   $(this).val('Please wait...');
		   $(this).closest('form').submit();
	   });
   });
</script>
