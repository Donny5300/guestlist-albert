<section id="front-page-block" class="hidden-print">
    <div class="front-page-background">
        <img src="{{ url('banners/image1.PNG') }}" class="img-responsive img-center" alt=""/>
    </div>
</section>

<section id="feature">
    <div class="container">
        <div class="center wow fadeInDown">
            <h2>Event {{ event.title }}</h2>

        </div>

        <div class="row">
            <div class="col-sm-12">
                {#{{Form::open(['method' => 'POST', 'route' => ['admin.events.show', event.id]])}}#}

				<div class="row hidden-print">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-3">
                                Search for:
                            </div>
                            <div class="col-sm-9">
								<input type="text" class="form-control" name="zoek" value="{{ Old('zoek') }}">
                            </div>

                        </div>

                    </div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-3">
                                <button class="form-control">Search</button>
                            </div>
                        </div>
                    </div>

                </div>
				{#{{Form::close()}}#}

            </div>
            <hr/>
            <div class="col-sm-12">
                <table class="table table-responsive table-striped">
                    <tr>
                        <td>Name</td>
                        <td>Phone number</td>
                        <td>Email</td>
                        <td>Gender</td>
                        <td class="hidden-print">Options</td>
                    </tr>

					{% for guest in event.grabGuestlist %}

						<tr class="{{ guest.is_accepted ? 'accepted' : '' }}">
                                <td>{{ guest.name }} {{ guest.last_name }}</td>
                                <td>{{ guest.phonenumber }}</td>
                                <td>{{ guest.email }}</td>
                                <td>{{ guest.gender == 'f' ? 'Female' : 'Male' }}</td>

                                <td class="hidden-print">
									{% if not guest.checkedin %}
										<a href="{{ url('admin/guestlist/'~guest.id~'/check?checkin=1') }}">Check in</a>

									{% else %}
										<a class="green-link" href="{{ url('admin/guestlist/'~guest.id~'/check?checkin=0') }}">
											Check out
										</a>
									{% endif %}
									| <a href="{{ url('admin/guestlist/'~guest.id~'/destroy') }}">Delete</a>
									{% if guest.is_accepted %}
										<span>| This guest is accepted</span>
									{% else %}
										| <a href="{{ url('/admin/events/'~guest.id~'/handle?accepted=1') }}">Accept</a>
										| <a href="{{ url('/admin/events/'~guest.id~'/handle?accepted=0') }}">Decline</a>
									{% endif %}


                                </td>
                            </tr>
					{% endfor %}
                </table>
            </div>
        </div>

		<!--/.row-.
	</div>
	<!--/.container-.
</section><!--/#feature-.

<style>
	.accepted {
		background: green;
	}
</style>