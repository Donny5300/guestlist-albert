<section id="feature">
    <div class="container">
        <div class="center wow fadeInDown">
            <h2>New event</h2>

			{% if session_has('message') %}
				{{ session('message') }}
			{% endif %}

			{% if errors.messages() %}
			<p class="lead">
					<ul style="list-style: none">
						{% for error in errors.messages() %}
						    <li>{{ error[0] }}</li>
						{% endfor %}
					</ul>
			</p>
			{% endif %}

        </div>

        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <div class="row">
                    <form action="/admin/events/create" class="form-horizontal" method="post">
						{{ csrf_field() }}
						<div class="features">
							<div class="col-sm-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
								<div class="feature-wrap">
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">
											Title</label>

										<div class="col-sm-9">
											<input type="text" class="form-control" value="{{ Old('title', item.title) }}" name="title">
										</div>
									</div>
								</div>
							</div>
							<!--/.col-md-4-->

							<div class="col-sm-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
								<div class="feature-wrap">
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">
											Date</label>

										<div class="col-sm-9">
											<input type="text" class="form-control" value="{{ Old('date', item.date) }}" name="date">
										</div>
									</div>
								</div>
							</div>
							<!--/.col-md-4-->

							<div class="col-sm-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
								<div class="feature-wrap">
									<div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">
											Personal description</label>

										<div class="col-sm-9">
											<textarea name="body" id="" cols="30" rows="10" class="form-control">{{ Old('body', item.body) }}</textarea>
										</div>
									</div>
								</div>
							</div>
							<!--/.col-md-4-->


							<div class="col-sm-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
								<div class="feature-wrap">
									<div class="form-group">

										<div class="col-sm-9">
											<input type="submit" class="btn btn-success" value="Save">
										</div>
									</div>
								</div>
							</div>
							<!--/.col-md-4-->
						</div>
						<!--/.services-->

					</form>

                </div>
            </div>
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</section><!--/#feature-->
