<section id="feature" style="min-height:700px">
    <div class="container">
        <div class="center wow fadeInDown">
            <h2>Event beheer</h2>
        </div>

        <div class="row">
			{% if session('message') %}
				<div class="col-sm-12 text-center">
					{{ session('message') }}
				</div>
			{% endif %}
			<div class="col-sm-12">
                    <table class="table table-responsive table-striped">
                        <tr>
                            <td>Event</td>
                            <td>Date</td>
                            <td>Amount signups</td>
                            <td>Options</td>
                        </tr>

						{% for event in events %}
							<tr>
                                <td>{{ event.title }}</td>
                                <td>{{ event.date.format('d-m-Y') }}</td>
                                <td>{{ event.grabGuestlist | length }}</td>
                                <td>
                                    <a href="{{ url('/admin/events/'~ event.id~'/show') }}">Show</a>
                                    |
                                    <a href="{{ url('admin/events/'~event.id~'/edit') }}">Edit</a>


									| <a href="{{ url('admin/events/'~event.id~'/delete') }}">Delete</a>

									|

									{% if event.lock == 1 %}
										<a href="{{ url('admin/events/'~event.id~'/lock') }}">Unlock</a>
									{% else %}
										<a href="{{ url('admin/events/'~event.id~'/lock') }}">Lock</a>
									{% endif %}
                                </td>
                            </tr>
						{% endfor %}
                    </table>

            </div>
        </div>

		<!--/.row-->
    </div>
	<!--/.container-->
</section><!--/#feature-->
