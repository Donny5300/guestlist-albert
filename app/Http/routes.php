<?php


	Route::get( '/', 'HomeController@index' );
	Route::post( '/sign', 'HomeController@storeVisitor' );


	Route::group( [ 'prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth' ], function ()
	{
		Route::get( '/', 'HomeController@index' );

		Route::get( 'events/', [ 'as' => 'admin.events.index', 'uses' => 'PartyController@index' ] );

		Route::get( 'events/create', [ 'as' => 'admin.events.create', 'uses' => 'PartyController@create' ] );
		Route::post( 'events/create', [ 'as' => 'admin.events.store', 'uses' => 'PartyController@store' ] );

		Route::get( 'events/{any}/show', [ 'as' => 'admin.events.show', 'uses' => 'PartyController@show' ] );
		Route::post( 'events/{any}/show', [ 'as' => 'admin.events.show', 'uses' => 'PartyController@show' ] );

		Route::get( 'events/{any}/edit', [ 'as' => 'admin.events.edit', 'uses' => 'PartyController@edit' ] );
		Route::post( 'events/{any}/edit', [ 'as' => 'admin.events.update', 'uses' => 'PartyController@update' ] );
		Route::get( 'events/{any}/handle', 'GuestlistController@handle' );


		Route::get( 'guestlist/{any}/check', [ 'as' => 'admin.guestlist.check', 'uses' => 'GuestlistController@checkedIn' ] );
		Route::get( 'events/{any}/lock', [ 'as' => 'admin.events.lock', 'uses' => 'PartyController@lock' ] );
		Route::get( 'events/{any}/delete', [ 'as' => 'admin.events.delete', 'uses' => 'PartyController@destroy' ] );

		Route::get( 'guestlist/{any}/destroy', 'GuestlistController@destroy' );
	} );


	Route::group( [ 'namespace' => 'Website' ], function ()
	{
		Route::get( 'login', 'UsersController@login' );
		Route::post( 'login', 'UsersController@doLogin' );
		Route::get( 'forgot_password', 'UsersController@forgotPassword' );
		Route::post( 'forgot_password', 'UsersController@doForgotPassword' );
		Route::get( 'reset_password/{token}', 'UsersController@resetPassword' );
		Route::post( 'reset_password', 'UsersController@doResetPassword' );
		Route::get( 'logout', 'UsersController@logout' );
	} );
