<?php namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;

class UsersController extends BaseController
{
	public $master = 'website.base.master';
	public $view_path = 'website.users';

	public function login()
	{
		return $this->output();
	}

	public function doLogin()
	{
		$request = app( 'request' );

		if( auth()->attempt( $request->only( [ 'email', 'password' ] ) ) )
		{
			return redirect()->to( '/' );
		}

		return redirect()->back()->with( 'message', 'Auth failed!' );

	}

	public function logout(  )
	{
		auth()->logout();

		return redirect()->to('/');
	}

}
