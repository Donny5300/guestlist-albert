<?php namespace App\Http\Controllers;


use App\Http\Controllers\Website\BaseController;
use App\Http\Requests\StoreVisitorRequest;
use App\Models\Guestlist;
use Carbon\Carbon;
use App\Models\Party;

/**
 * Class HomeController
 *
 * @package App\Http\Controllers
 */
class HomeController extends BaseController
{

	/**
	 * @var string
	 */
	public $view_path = 'website.home';
	/**
	 * @var Party
	 */
	private $event;
	/**
	 * @var Guestlist
	 */
	private $list;

	/**
	 * HomeController constructor.
	 *
	 * @param Party     $party
	 * @param Guestlist $list
	 */
	public function __construct( Party $party, Guestlist $list )
	{
		$this->event = $party;
		$this->list  = $list;
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		$today  = Carbon::now()->toDateString();
		$events = $this->event->where( 'date', '>=', $today )->where( 'lock', 0 )->get();;

		return $this->output( null, compact( 'events' ) );
	}

	/**
	 * @param StoreVisitorRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function storeVisitor( StoreVisitorRequest $request )
	{
		if( $this->list->addItem( $request ) )
		{
			return redirect()->back()->with( 'message', 'Thanks! Your name has been registered to my guest list. Don\'t forget to follow me on Social Media!' );

		}

		return redirect()->back()->with( 'message', 'Something went wrong :(' );
	}

}
