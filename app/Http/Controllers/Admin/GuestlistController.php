<?php namespace App\Http\Controllers\Admin;

use App\Models\Guestlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

/**
 * Class GuestlistController
 *
 * @package App\Http\Controllers\Admin
 */
class GuestlistController extends BaseController
{
	/**
	 * @var string
	 */
	public $master = 'website.base.master';
	/**
	 * @var string
	 */
	public $view_path = 'admin.parties';

	/**
	 * GuestlistController constructor.
	 *
	 * @param Guestlist $guest
	 */
	public function __construct( Guestlist $guest )
	{
		$this->guest = $guest;

	}

	/**
	 * @param Request $request
	 * @param         $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function handle( Request $request, $id )
	{
		$guest              = $this->guest->find( $id );
		$guest->is_accepted = $request->accepted;
		$guest->save();

//		dd('File: ' . __FILE__, 'Line: '. __LINE__ , $guest->event);

		if( $guest->is_accepted )
		{
			Mail::send( 'emails.accepted_guest', [ 'guest' => $guest ], function ( $message ) use ( $guest )
			{
				$message->to( $guest->email, $guest->name . ' ' . $guest->last_name )->subject( 'Albert | Je aanvraag is geaccepteerd!' );
			} );
		}

		return redirect()
			->back()
			->with( 'message', 'Guest is accepted!' );
	}

	/**
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroy( $id )
	{
		$this->guest->find( $id )->delete();

		return redirect()
			->back()
			->with( 'message', 'Guest is removed!' );
	}

	/**
	 * @param Request $request
	 * @param         $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function checkedIn( Request $request, $id )
	{
		$guest            = $this->guest->find( $id );
		$guest->checkedin = $request->checkin;
		$guest->save();

		return redirect()
			->back()
			->with( 'message', 'Guest is checkin!' );
	}
}
