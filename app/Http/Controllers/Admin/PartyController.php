<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests\StorePartyRequest;
use App\Models\Party;
use Illuminate\Contracts\Cache\Store;

class PartyController extends BaseController
{
	public $master = 'website.base.master';
	public $view_path = 'admin.parties';

	public function __construct( Party $party )
	{
		$this->party = $party;
	}

	public function index()
	{
		$events = $this->party->all();

		return $this->output( null, compact( 'events' ) );
	}

	public function create()
	{
		return $this->output();
	}

	public function store( StorePartyRequest $request )
	{
		$message = 'Could not store this event';
		if( $this->party->addNewOrUpdate( $request ) )
		{
			$message = 'Event is stored';
		}

		return redirect()->back()->with( 'message', $message );
	}

	public function edit( $id )
	{
		$item = $this->party->find( $id );

		return $this->output( null, compact( 'item' ) );

	}

	public function update( StorePartyRequest $request, $id )
	{

		$message = 'Could not store this event';

		if( $this->party->addNewOrUpdate( $request, $id ) )
		{
			$message = 'Event is updated';
		}

		return redirect()->to( 'admin/events' )->with( 'message', $message );
	}

	public function show( $id )
	{
		$event = $this->party->with( 'grabGuestlist' )->find( $id );

		return $this->output( null, compact( 'event' ) );
	}

	public function lock( $id )
	{
		$item       = $this->party->find( $id );
		$locked     = !( $item->lock );
		$item->lock = $locked;

		$message = 'Event is locked. No registration possible anymore.';
		if( !$locked )
		{
			$message = 'Event is unlocked';
		}
		$item->save();

		return redirect()->back()->with( 'message', $message );

	}

	public function handle( $id )
	{

	}

	public function destroy( $id )
	{
		$this->party->find( $id )->delete();

		return redirect()->back()->with( 'message', 'Event is deleted!' );

	}
}
