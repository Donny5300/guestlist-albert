<?php namespace App\Http\Controllers\Admin;

class HomeController extends BaseController
{
	public $master = 'website.base.master';
	public $view_path = 'admin.home';

	public function index()
	{
		return $this->output();
	}
}
