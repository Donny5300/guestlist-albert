<?php

	namespace App\Http\Controllers;

	use Illuminate\Foundation\Bus\DispatchesJobs;
	use Illuminate\Routing\Controller as BaseController;
	use Illuminate\Foundation\Validation\ValidatesRequests;
	use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
	use Illuminate\Foundation\Auth\Access\AuthorizesResources;
	use Illuminate\Support\Facades\Route;

	class Controller extends BaseController
	{
		use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

		public $view_path = false;
		public $master = 'website.base.master';

		protected function back( $errors = [ ], $name = null )
		{
			return redirect()->back()->withInput()->withErrors( Session::get( 'validation_errors' ) );
		}

		protected function redirect( $msg, $route = null )
		{
			$to = empty( $route ) ? $this->redirect_to : $route;

			if( !is_array( $to ) )
			{
				$to = [ $to ];
			}

			return call_user_func_array( [ 'Redirect', 'route' ], $to )
				->with( 'msg', $msg );
		}

		protected function output( $view = false, $data = [ ] )
		{

			if( $view == false || null )
			{
				$route = Route::currentRouteAction();

				$view = substr( $route, strpos( $route, "@" ) + 1 );
			}

			$template = ( $this->view_path ? $this->view_path . '.' : '' ) . $view;

			$renderTemplate = view($template, $data);

			$data['view'] = $renderTemplate;

			return view( $this->master, $data );
		}

		protected function handleException( Exception $e )
		{
			if( $e instanceof ValidationException )
			{
				$errors = $e->getErrors();
			}
			else
			{
				$errors = $e->getMessage();
			}

			return $this->back( $errors );
		}

		public function inputAll()
		{

			$input = Input::except( [ '_token', '_method' ] );

			return $input;
		}
	}
