<?php

	namespace App\Http\Requests;

	use Illuminate\Foundation\Http\FormRequest;

	/**
	 * Class StorePartyRequest
	 *
	 * @package App\Http\Requests
	 */
	class StoreVisitorRequest extends Request
	{
		/**
		 * @return bool
		 */
		public function authorize()
		{
			return true;
		}

		/**
		 * @return array
		 */
		public function rules()
		{
			return [
				'event_id'    => 'required|numeric',
				'birthday'    => 'required|date_format:d-m-Y',
				'name'        => 'required',
				'email'       => 'required|email',
				'last_name'   => 'required',
				'phonenumber' => 'numeric',
				'gender'      => 'required',
				'amount'      => 'numeric'
			];
		}
	}
