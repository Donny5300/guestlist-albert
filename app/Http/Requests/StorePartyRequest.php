<?php

	namespace App\Http\Requests;

	use Illuminate\Foundation\Http\FormRequest;

	/**
	 * Class StorePartyRequest
	 *
	 * @package App\Http\Requests
	 */
	class StorePartyRequest extends Request
	{
		/**
		 * @return bool
		 */
		public function passesAuthorization()
		{
			return true;
		}

		/**
		 * @return array
		 */
		public function rules()
		{
			return [
				'title' => 'required|min:3',
				'date'  => 'required|date_format:d-m-Y',
			];
		}

		/**
		 * @return array
		 */
		public function messages(  )
		{
			return [
				'date.date_format' => 'Date must be dd-mm-yyyy'
			];
		}
	}
