<?php namespace App\Models;

use Eloquent;

class Party extends Eloquent
{

	protected $fillable = [ 'title', 'date', 'body', 'image', 'gender_lock' ];

	public function getDates()
	{
		return [ 'date' ];
	}


	public function setDateAttribute( $date )
	{
		if( $date )
		{
			return $this->attributes['date'] = date( 'Y-m-d', ( strtotime( $date ) ) );
		}

		return $this->attributes['date'] = '';
	}

	public function grabGuestlist()
	{
		return $this->hasMany( Guestlist::class, 'event_id' );
	}

	public function addNewOrUpdate( $request, $id = null )
	{
		$item = $this->firstOrNew( [ 'id' => $id ] );

		$item->title = $request->title;
		$item->date  = $request->date;
		$item->body  = $request->body;

		if( $item->save() )
		{
			return $item;
		}

		return false;
	}
}