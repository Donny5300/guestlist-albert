<?php namespace App\Models;

use Eloquent;
use App\Models\Party;

/**
 * Class Guestlist
 *
 * @package App\Models
 */
class Guestlist extends Eloquent
{
	/**
	 * @var array
	 */
	protected $fillable = [ 'name', 'last_name', 'email', 'phonenumber', 'birthday', 'gender', 'event_id' ];

	/**
	 * @return array
	 */
	public function getDates()
	{
		return [ 'birthday' ];
	}

	/**
	 * @param $date
	 */
	public function setBirthdayAttribute( $date )
	{
		if( $date )
		{
			$this->attributes['birthday'] = date( 'Y-m-d', ( strtotime( $date ) ) );
		}
		else
		{
			$this->attributes['birthday'] = '';
		}
	}

	/**
	 * @param $request
	 * @return Guestlist
	 */
	public function addItem( $request )
	{
		$item              = new self;
		$item->phonenumber = $request->phonenumber;
		$item->birthday    = $request->birthday;
		$item->name        = $request->name;
		$item->email       = $request->email;
		$item->last_name   = $request->last_name;
		$item->event_id    = $request->event_id;
		$item->gender      = $request->gender;

		if( $item->save() )
		{
			return $item;
		}

		return $item;
	}

	public function event()
	{
		return $this->belongsTo( Party::class );
	}

}