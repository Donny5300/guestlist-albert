<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGueslistsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('guestlists', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('name');
			$table->date('birthday');
			$table->string('email');
			$table->string('phonenumber');
			$table->integer('event_id');
			$table->string('gender');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gueslists');
	}

}
