<?php

	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class UpdateGuestlistTable extends Migration
	{

		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
			Schema::table( 'guestlists', function ( Blueprint $table )
			{
				$table->boolean( 'is_accepted' );
			} );
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
			Schema::table( 'guestlists', function ( Blueprint $table )
			{
				$table->dropColumn( 'is_accepted' );
			} );
		}

	}
